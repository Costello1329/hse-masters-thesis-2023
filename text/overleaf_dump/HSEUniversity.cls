\NeedsTeXFormat{LaTeX2e}

\LoadClass[fontsize=13bp]{scrreprt}

\RequirePackage{ifpdf}
\RequirePackage{kvoptions}
\RequirePackage[a4paper,left=20mm,right=20mm,top=20mm,bottom=25mm,footskip=10mm]{geometry}

\RequirePackage{cmap}						% Улучшенный поиск русских слов в полученном pdf-файле
\RequirePackage[T2C]{fontenc}				% Поддержка русских букв
\RequirePackage[utf8]{inputenc}				% Кодировка utf8
\RequirePackage{csquotes}
\RequirePackage[russian]{babel}	% Языки: русский, английский

\RequirePackage{multirow} 
\RequirePackage{booktabs}


\RequirePackage[font=small,skip=4pt]{caption}
\RequirePackage{graphicx}

% \usepackage{pscyr}						% Красивые русские шрифты
% \renewcommand{\rmdefault}{ftm} % Включаем Times New Roman

%%% Оформление абзацев %%%
\RequirePackage{indentfirst} % Красная строка

%%% Размер шрифатов у заголовков %%%
\RequirePackage{titlesec}

%%% Table of Contents %%%
\RequirePackage{tocloft}
\RequirePackage{titletoc}
\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{mathtools}
\RequirePackage{float}
\RequirePackage{systeme}

% Алгоритм
\usepackage[ruled]{algorithm2e}

% Перевод плагина
\SetKwInput{KwData}{Исходные параметры}
\SetKwInput{KwResult}{Результат}
\SetKwInput{KwIn}{Входные данные}
\SetKwInput{KwOut}{Выходные данные}
\SetKwIF{If}{ElseIf}{Else}{если}{тогда}{иначе если}{иначе}{конец условия}
\SetKwFor{While}{до тех пор, пока}{выполнять}{конец цикла}
\SetKw{KwTo}{от}
\SetKw{KwRet}{возвратить}
\SetKw{Return}{возвратить}
\SetKwBlock{Begin}{начало блока}{конец блока}
\SetKwSwitch{Switch}{Case}{Other}{Проверить значение}{и выполнить}{вариант}{в противном случае}{конец варианта}{конец проверки значений}
\SetKwFor{For}{цикл}{выполнять}{конец цикла}
\SetKwFor{ForEach}{для каждого}{выполнять}{конец цикла}
\SetKwRepeat{Repeat}{повторять}{до тех пор, пока}
\SetAlgorithmName{Алгоритм}{алгоритм}{Список алгоритмов}

\usepackage{tabularx}



\RequirePackage[linktocpage=true,plainpages=false,pdfpagelabels=false]{hyperref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Библиография %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[
	style=gost-numeric-min,
% 	language=russian,
    language=auto,
	autolang=other,
	backend=biber,
	defernumbers=true,% Нумерация начинается как только встречается группа.
	sorting=none,
	url=false,
	doi=false,
	isbn=false,
	movenames=false,
	maxnames=3
]{biblatex}


\renewcommand*{\multicitedelim}{\addcomma\space}

% Настройки языка в списке литературы.
\DeclareSourcemap{
	\maps[datatype=bibtex,overwrite=false]{
	 \map{
			\step[fieldset=language,fieldvalue=english]
	 }
	\map{
		\step[fieldsource=language,match=\regexp{English},replace=\regexp{english}]
	}
	\map{
		\step[fieldsource=language]
		\step[fieldset=hyphenation,origfieldval]
	}
	}
}
%%%%%%%%%%%%%%%%%%%%%%
\NewBibliographyString{langjapanese}
\NewBibliographyString{fromjapanese}

\newcommand{\putbibliography}{
	\chapter*{Список использованных источников}
	
	\printbibliography[heading=none]

% 	\printbibliography[env=gostbibliography,heading=none] 
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Установка основных характеристик текста

\sloppy				% Избавляемся от переполнений
\clubpenalty=10000		% Запрещаем разрыв страницы после первой строки абзаца
\widowpenalty=10000		% Запрещаем разрыв страницы после последней строки абзаца

\linespread{1.25} % Полуторный интервал
\setlength{\parindent}{12.5mm} % Отступ красной строки

\captionsetup[table]{position=t,singlelinecheck=false, justification=centering,labelsep=period}
\captionsetup[figure]{labelfont={bf},textfont={bf},position=b,singlelinecheck=false,labelsep=period,justification=centering}


%%% Главы и разделы %%%
\titleformat{\chapter}
  {\Large\bfseries\centering}
  {\chaptertitlename\ \thechapter.}{4pt}{}
\titlespacing*{\chapter}{0pt}{0pt}{12pt}
\titleformat{\section}
  {\large\bfseries\centering}
  {\thesection.}{4pt}{}
\titlespacing*{\section}{0pt}{12pt}{6pt}
\titleformat{\subsection}
  {\centering \bfseries}
  {\thesubsection.}{3pt}{}
\titlespacing*{\subsection}{0pt}{8pt}{4pt}
\titleformat{\subsubsection}
  {\centering \bfseries}
  {\thesubsubsection.}{3pt}{}
\titlespacing*{\subsubsection}{0pt}{8pt}{4pt}



\renewcommand{\cftchapdotsep}{\cftdotsep}
\let\@originalchapter\chapter
\def\chapter{
	 \@ifstar\chapterstar\@originalchapter
 }
\def\chapterstar{
	 \@ifstar\chapter@nonum@notoc\chapter@nonum
 }
\newcommand\chapter@nonum@notoc[2][]{
   \@originalchapter*{#2}
}
\newcommand\chapter@nonum[2][]{
   \@originalchapter*{#2}
   \addcontentsline{toc}{chapter}{#2}
}
\titlecontents{chapter}% <section-type>
  [0pt]% <left>
  {}% <above-code>
  {\bfseries\chaptername\ \thecontentslabel.\enskip}% <numbered-entry-format>
  {}% <numberless-entry-format>
  {\bfseries\dotfill\contentspage}% <filler-page-format>

%%%%%%%%%%%%%%%%%%%%%%%%
% Обработка опций пакета

\SetupKeyvalOptions{
	family=HSE,
	prefix=HSE@
}

\def\@facultyString{Факультет компьютерных наук}
\def\@typeString{Выпускная квалификационная работа на тему}
\def\@specName{Науки о данных}
\def\@specCode{01.04.02 Прикладная математика и информатика}
\newif\ifHasReviewer
\HasReviewerfalse

\newcommand{\titleENG}[1]{
	\def\@titleENG{#1}
}
\newcommand{\Year}[1]{\def\@year{#1}}
\newcommand{\supervisor}[2]{
	\def\@supervisorTitle{#1}
	\def\@supervisorName{#2}
}
\newcommand{\reviewer}[2]{
	\def\@reviewerTitle{#1}
	\def\@reviewerName{#2}
}
\newcommand{\Abstract}[1]{
	\def\@abstract{#1}
}
\newcommand{\authorInitials}[1]{
	\def\@authorInitials{#1}
}


\renewcommand{\maketitle}{
{
	\thispagestyle{empty}

	\centering

	{
	    \textbf{Федеральное государственное автономное образовательное учреждение высшего профессионального образования <<Национальный исследовательский университет \\ 
	        <<Высшая школа экономики>>}
	}

	\vfill

	\@facultyString

	\vfill
	
	\@author

	\vfill
	
	\@typeString

	\textbf{\MakeUppercase{\@title}}
	
	\textbf{\MakeUppercase{\@titleENG}}

	\vfill

    по направлению подготовки \textit{\underline{\@specCode}} \\
    образовательная программа <<\@specName>>

	\vfill
	\vfill
	\vfill

	\begin{minipage}[t]{0.45\textwidth}
			Студент \\
% 			\@reviewerTitle
            

    		\vspace{11mm}
    
    		\hrule
    		\vspace{3mm}
    
    		\@authorInitials

% 			\@reviewerName
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.45\textwidth}
		Научный руководитель \\
		\@supervisorTitle

		\vspace{10mm}

		\hrule
		\vspace{3mm}

		\@supervisorName
	\end{minipage}

	\vfill
	\vfill
	\vfill

	Москва, \@year{} год

	\newpage
}
}

% Уменьшаем отступы в перечислениях

\newenvironment{itemize*}
  {\begin{itemize}%
    \setlength{\itemsep}{1pt}
    \setlength{\parskip}{1pt}}
  {\end{itemize}}

\newenvironment{enumerate*}
  {\begin{enumerate}%
    \setlength{\itemsep}{1pt}
    \setlength{\parskip}{1pt}}
  {\end{enumerate}}
  