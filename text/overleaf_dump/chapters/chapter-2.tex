\chapter{Разработка генератора синтетических данных для тестирования алгоритмов, учитывающих структуру вкладов кластеров}

\begin{definition}
Дано множество кластеров $S$. Относительным вкладом кластера $S_0 \in S$ называется величина $\frac{w\left(S_0\right)}{\sum_{k = 1}^{\abs{S}}{w\left(S_k\right)}}$, где $w\left(S_k\right)$ – вклад кластера $S_k$.
\end{definition}

\section{Постановка задачи}

\begin{problem}
Создать генератор кластерной структуры, учитывающий требуемые ограничения на размеры, форму и вклад каждого кластера. Входные параметры должны содержать:

\begin{itemize}
    \item $n$ – размерность пространства
    \item $K$ – количество кластеров
    \item $\left\{\left(s_i, w_i\right)\right\}_{i \in \left[1; K\right]}$ – набор пар размеров кластеров и их \textit{относительных} вкладов
    \item Непрерывное распределение $d$, определяющее форму кластеров.
    \item Параметр $\alpha \in \left(0, 1\right)$, определяющий степень смешения (накладывания) кластеров друг на друга. Формально, требуется чтобы у каждого кластера доля ближайших к его центру точек, \textit{относящихся только к нему} была не меньше, чем $\alpha$. Назовём это требование $\alpha$-условием
\end{itemize}

\noindent В качестве выходных данных, от генератора ожидается:

\begin{itemize}
    \item $P$ – матрица данных
    \item $\left\{\left(S_i, \vec{c_i}\right)\right\}_{i \in \left[1; K\right]}$ – эталонное разбиение данных на кластеры и их центры
\end{itemize}
\end{problem}

\section{Разработка модели и метода генерации данных}

В данном разделе предлагается модель генератора данных, решающая \texttt{задачу 1}.

\subsection{Общая схема: жадная генерация кластеров}
Будем размещать кластеры в пространстве последовательно, пока не разместим требуемого количества. На каждой итерации будем следить за соблюдением $\alpha$-условия, ограничивающего степень наложения кластеров друг на друга.

Разделим итерацию на две части: генерация кластера и размещение его в пространстве. На первом этапе фиксируются его форма, количество точек в нём и их расположение. На втором – положение его центра в пространстве, а следовательно и абсолютное значение его вклада.

\subsection{Эллипсоидные (гауссовские) кластеры}

\begin{definition}[Специальная ортогональная матрица, группа $SO\left(n\right)$]
    Ортогональную матрицу, определитель которой равен единице, называют специальной ортогональной. Группу специальных ортогональных матриц размера $n$ обозначают $SO\left(n\right)$.
\end{definition}

\begin{definition}[Эллипсоид]
Пусть $D \succ 0$ – диагональная матрица, $V$ – специальная ортогональная матрица (матрица из группы $SO\left(n\right)$), а $\vec{c}$ – произвольный $n$-вектор.

Тогда эллипсоид в $n$-мерном пространстве может быть представлен в следующем удобном для нас виде:

\begin{equation}
    El_n\left(D, V, \vec{c}\right) = \{\vec{x}\ |\ \left(\vec{x} - \vec{c}\right)^T \left(V^T D^2 V\right)^{-1} \left(\vec{x} - \vec{c}\right) \le 1\}
\end{equation}

Эквивалентно:

\begin{equation}
El_n\left(D, V, \vec{c}\right) = \{\vec{x}\ |\ \norm{\left(VD\right)^{-1} \left(\vec{x} - \vec{c}\right)} \le 1\}
\end{equation}
\end{definition}

Каждому кластеру поставим в соответствие $n$-мерное нормальное распределение $N\left(\vec{\mu}, \Sigma\right)$ и $n$-мерный эллипсоид $El_n\left(D, V, \vec{\mu}\right)$, подобранный таким образом, чтобы его центр совпадал с параметром $\vec{\mu}$ распределения, а $\int_{\vec{x} \in El_n\left(D, V, \vec{\mu}\right)} p_{N\left(\vec{\mu}, \Sigma\right)}\left(\vec{x}\right) d\vec{x} = \alpha$.

\begin{definition}[Коллайдер]
    Этот эллипсоид будем называть $\alpha$-коллайдером соответствующего ему распределения.
\end{definition}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{imgs/collider_example.png}
    \caption{Пример коллайдера при $n = 2$ и $\alpha = 0.85$}
    \label{fig:mesh1}
\end{figure}

\begin{theorem}[О коллайдере]
    Пусть $\left\{d_i\right\}_{i = 1}^{i = n}$ – набор положительных вещественных чисел, $D$ – диагональная матрица, составленная из них, а $V$ – случайная матрица из $SO\left(n\right)$. Тогда $El_n\left(t\left(\alpha\right) \cdot D, V, \vec{\mu}\right)$ – коллайдер распределения $N\left(\vec{0}, V D^2 V^T\right)$, где $t\left(\alpha\right) = \sqrt{2 \gamma_r^{-1}\left(\frac{n}{2}, \alpha\right)}$, а $\gamma_r^{-1}\left(a, y\right)$ – обратная функция (по второму аргументу) к регуляризованной неполной нижней гамма-функции.
\end{theorem}

\begin{note}
Регуляризованной неполной нижней гамма-функцией называют функцию $\gamma_r\left(a, x\right) = \frac{1}{\Gamma\left(a\right)} \int_0^x{t^{a-1}e^{-t}dt} \in \left[0, 1\right]$. Фунция $\gamma_r^{-1}\left(a, y\right)$ называется обратной функцией (по второму аргументу) к $\gamma_r\left(a, x\right)$, то есть $\forall y \in \left[0, 1\right] : y = \gamma_r\left(a, \gamma_r^{-1}\left(a, y\right)\right)$.
\end{note}

\begin{proof}
Получим результат в частном случае $El_n\left(t\left(\alpha\right) \cdot D, V, \vec{\mu}\right) = El_n\left(t\left(\alpha\right) \cdot E, E, \vec{0}\right) = \mathbb{B}_n\left(t\left(\alpha\right), \vec{0}\right)$, где $E$ – единичная матрица, а $\mathbb{B}_n\left(r, \vec{c}\right)$ – $n$-шар радиуса $r$ с центром в $\vec{c}$. Общий же случай следует из него после применения преобразования, обратного к описанному в \texttt{следствии теоремы 4}.

\noindent Распишем интересующий нас интеграл с применением перехода в $n$-мерную полярную систему координат, определение которой приведено в \cite{8} и \cite{9}. Также используем результат для определения объёмного элемента $d^nV$ в этой системе координат, приведённый в \cite{9}.

\begin{gather*}
    \int_{\vec{x} \in \mathbb{B}_n\left(t\left(\alpha\right), \vec{0}\right)}{p_{N\left(\vec{\mu}, \Sigma\right)}\left(\vec{x}\right) d^nV} = \\ \frac{1}{\left(2 \pi\right)^\frac{n}{2}} \int_{0}^{t\left(\alpha\right)} \int_{0}^{\pi} \dots \int_{0}^{\pi} \int_{0}^{2\pi} e^{-\frac{r^2}{2}} r^{n-1} \sin^{n-2}\left(\varphi_1\right) \sin^{n-3}\left(\varphi_2\right) \cdots \sin\left(\varphi_{n-2}\right) dr d\varphi_1 d\varphi_2 \cdots d\varphi_{n-1} = \\ \frac{1}{\left(2 \pi\right)^\frac{n}{2}} \int_{0}^{t\left(\alpha\right)} r^{n-1} e^{-\frac{r^2}{2}} dr \cdot \prod_{i=1}^{n-2} \int_0^{\pi} \sin^i\left(\varphi\right) d\varphi \cdot \int_0^{2\pi}d\varphi_{n-1} = \\ \frac{1}{\left(2\pi\right)^{\frac{n}{2} - 1}} \int_{0}^{t\left(\alpha\right)} r^{n-1} e^{-\frac{r^2}{2}} dr \cdot \prod_{i=1}^{n-2} \int_0^{\pi} \sin^i\left(\varphi\right) d\varphi
\end{gather*}

\noindent Обозначим $I = \int_{0}^{t\left(\alpha\right)} r^{n-1} e^{-\frac{r^2}{2}} dr$, $J_i = \int_0^{\pi} \sin^i\left(\varphi\right) d\varphi$ и $J = \prod_{i=1}^{n - 2}J_i$

\begin{gather*}
    I = \int_{0}^{t\left(\alpha\right)} r^{n-1} e^{-\frac{r^2}{2}} dr = 2^{\frac{n}{2} - 1}\int_{0}^{\frac{t^2\left(\alpha\right)}{2}} r^{n/2-1} e^{-r} dr = 2^{\frac{n}{2} - 1} \gamma\left(\frac{n}{2}, \frac{t^2\left(\alpha\right)}{2}\right)
\end{gather*}

\noindent В выражении сверху функция $\gamma\left(n, x\right)$ представялет нижнюю неполную гамма-функцию: $\gamma\left(n, x\right) = \int_0^x y^{n - 1} e^{-y} dy$

\noindent В анализе хорошо известна формула, доказательство которой приведено в \cite{7}

\begin{gather*}
    \int \sin^n\left(x\right) dx = -\frac{1}{n} \sin^{n-1}\left(x\right) \cos\left(x\right) + \frac{n-1}{n}\int \sin^{n-2}\left(x\right) dx
\end{gather*}

\noindent Используя её частный случай, получим:
\begin{gather*}
    J_i = \int_{0}^{\pi} \sin^i\left(x\right) dx = 0 + \frac{i-1}{i}\int_{0}^{\pi} \sin^{i-2}\left(x\right) dx = \frac{i-1}{i}J_{i-2} = \frac{\left(i-1\right)!!}{i!!} J_{i \mod 2}
\end{gather*}

\noindent В полученном результате $i \mod 2$ означает остаток от деления $i$ на $2$. С учётом $J_0 = \pi$ и $J_1 = 2$, обозначим $\xi_i = \pi I\left(i \mod 2 = 0\right) + 2 I\left(i \mod 2 = 1\right)$, где $I$ – индикатор-функция.

\noindent Получим значение $J$:
\begin{gather*}
    J = \prod_{i=1}^{n - 2}J_i = \frac{\left(n-3\right)!!}{\left(n - 2\right)!!} \frac{\left(n-4\right)!!}{\left(n - 3\right)!!} \cdots \frac{0!!}{1!!} \prod_{i = 1}^{n - 2}\xi_i = \frac{1}{\left(n - 2\right)!!} \prod_{i = 1}^{n - 2}\xi_i = \frac{1}{\left(n - 2\right)!!} \left(2 \pi\right)^{\lfloor\frac{n}{2} - 1\rfloor} 2^{n \mod 2}
\end{gather*}

\noindent Наконец:

\begin{gather*}
    \int_{\vec{x} \in \mathbb{B}_n\left(t\left(\alpha\right), \vec{0}\right)}{p_{N\left(\vec{\mu}, \Sigma\right)}\left(\vec{x}\right) d^nV} = \frac{1}{\left(2 \pi\right)^{\frac{n}{2} - 1}} I J = \\ \frac{1}{\left(2 \pi\right)^{\frac{n}{2} - 1}} 2^{\frac{n}{2} - 1} \gamma\left(\frac{n}{2}, \frac{t^2\left(\alpha\right)}{2}\right) \frac{1}{\left(n - 2\right)!!} \left(2 \pi\right)^{\lfloor\frac{n}{2} - 1\rfloor} 2^{n \mod 2} = \\ \frac{\pi^{-\frac{n \mod 2}{2}} 2^{\frac{n}{2} - 1 + \frac{n \mod 2}{2}}}{\left(n - 2\right)!!} \gamma\left(\frac{n}{2}, \frac{t^2\left(\alpha\right)}{2}\right)
\end{gather*}

\noindent Вычислим $H = \frac{\pi^{-\frac{n \mod 2}{2}} 2^{\frac{n}{2} - 1 + \frac{n \mod 2}{2}}}{\left(n - 2\right)!!}$.
Пусть $k \in \mathbb{N_+}$. Разберём два случая:

\begin{itemize}
    \item $n = 2k$: $H = \frac{2^{k - 1}}{\left(2k - 2\right)!!} = \frac{1}{\left(k - 1\right)!} = \frac{1}{\Gamma\left(k\right)} = \frac{1}{\Gamma\left(\frac{n}{2}\right)}$
    \item $n = 2k + 1$: $H = \frac{2^{k}}{\sqrt{\pi}\left(2k - 1\right)!!} = \frac{1}{\Gamma\left(k + \frac{1}{2}\right)} = \frac{1}{\Gamma\left(\frac{n}{2}\right)}$
\end{itemize}

\noindent Получаем $H = \frac{1}{\Gamma\left(\frac{n}{2}\right)}$. Финально:

\begin{gather*}
    \int_{\vec{x} \in \mathbb{B}_n\left(t\left(\alpha\right), \vec{0}\right)}{p_{N\left(\vec{\mu}, \Sigma\right)}\left(\vec{x}\right) d^nV} = \frac{\gamma\left(\frac{n}{2}, \frac{t^2\left(\alpha\right)}{2}\right)}{\Gamma\left(\frac{n}{2}\right)} = \gamma_r\left(\frac{n}{2}, \frac{t^2\left(\alpha\right)}{2}\right)
\end{gather*}

Наконец, подставив $t\left(\alpha\right) = \sqrt{2 \gamma_r^{-1}\left(\frac{n}{2}, \alpha\right)}$, получим:

\begin{gather*}
    \int_{\vec{x} \in \mathbb{B}_n\left(t\left(\alpha\right), \vec{0}\right)}{p_{N\left(\vec{\mu}, \Sigma\right)}\left(\vec{x}\right) d^nV} = \gamma_r\left(\frac{n}{2}, \frac{t^2\left(\alpha\right)}{2}\right) = \gamma_r\left(\frac{n}{2}, \gamma_r^{-1}\left(\frac{n}{2}, \alpha\right)\right) = \alpha
\end{gather*}

\end{proof}

\subsection{Процедура генерации одного кластера}

Опишем процедуру генерации гауссовского кластера, на выходе из которой мы желаем получить один единственный кластер с центром в нуле, повёрнутый \textit{случайно} в пространстве.

\begin{algorithm*}[H]
\caption{\texttt{SINGLE\_CLUSTER\_GENERATION(n, size, dist, alpha)}}

\begin{enumerate}
    \item Сгенерируем выборку $\left\{d_i\right\}_{i = 1}^{i = n}$ из распределения $d$ и составим диагональную матрицу $D$ из неё.
    \item * Сгенерируем случайную матрицу $V$ из группы $SO\left(n\right)$
    \item ** Сгенерируем выборку размера $s$ (требуемое количество точек внутри коллайдера) из многомерного нормального распределения $N\left(\vec{0}, V D^2 V^T\right)$. В качестве центра кластера положим $\vec{0}$.
    \item Сопоставим сгенерированному кластеру коллайдер согласно \texttt{теореме 1}
\end{enumerate}
\end{algorithm*}

\begin{note}
    * Для генерации случайной матрицы из $SO\left(n\right)$, можно использовать следующий подход:
    \begin{enumerate}
        \item Очевидно, $\exists i \in \left[1, n\right]: w_i > 0$ (на практике все компоненты вектора $\vec{w}$ ненулевые, хотя этого не требуется). Дополним этот вектор до линейно-независимого набора из $n$ векторов, добавив к нему $n-1$ вектор из множества столбцов единичной матрицы за исключением $i$-ого. Образуем матрицу $W$, используя полученный набор векторов в качестве её столбцов.
        
        \item Ортогонализуем полученную матрицу $W$, получив в результате матрицу $V \in SO\left(n\right)$. Сделать это можно, например, с использованием алгоритма ортогонализации Грама-Шмидта. Процесс ортогонализации производится так, чтобы определитель полученной матрицы был равен единице, вследствие чего её векторы образовали бы ортонормированный базис.
    \end{enumerate}
    Таким образом, мы получили процедуру генерации случайного нормированного вектора и дополнения его до ортонормированного базиса (любого из $2^{n-1} n!$ возможных).
\end{note}

\begin{note}
    ** Подобный выбор центра кластера приводит к определённым смещениям, так как в общем случае $\frac{\sum_{\vec{p} \in S}{\vec{p}}}{\abs{S}} \neq \vec{0}$. Однако на практике при $d \sim U\left[0.5, 1.5\right]$, смещения незначительны (меньше $5\%$) уже при $\abs{S} = 300$. Ниже приведён график зависимости $\frac{\norm{\overline{S}}}{\max_{\vec{p} \in S}{\norm{\vec{p}}}}$ от $\abs{S}$ при $n \in \left[2, 10\right]$ (в качестве каждой точки взято среднее из $1000$ экспериментов, проведённых автором).
\end{note}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.666\textwidth]{imgs/points_gen_example.png}
    \caption{График зависимости $\frac{\norm{\overline{S}}}{\max_{\vec{p} \in S}{\norm{\vec{p}}}}$ от $\abs{S}$}
    \label{fig:mesh1}
\end{figure}

\subsection{Процедура размещения кластера в пространстве}

Применим процедуру \texttt{SINGLE\_CLUSTER\_GENERATION} и разместим кластеры в пространстве так, чтобы их коллайдеры не пересекались (это обеспечит выполнение $\alpha$-условия). Также в процессе размещения кластеров будем следить за тем, чтобы относительные вклады кластеров соответствовали заданным.

\begin{algorithm*}[H]
\caption{\texttt{GENERATE\_CLUSTERS(n, sizes, weights, dist, alpha, probes)}}

\begin{enumerate}
    \item Определим величину $\tau = 1$, называемую коэффициентом растяжения
    \item Для каждого $i \in \left[1, K\right]$:
    \begin{enumerate}
        \item Сгенерируем кластер $S_i$ размера $s_i$ при помощи вышеописанной процедуры
        \item Объявим пустой список $\tau_{probes}$
        \item Повторим не более $probes$ раз следующие действия:
            \begin{enumerate}
                \item Сгенерируем случайный вектор $\vec{c_{p}} \sim \mathbb{S}_{n-1}\left(\sqrt{\frac{\tau w_i}{s_i}}\right)$.
                
                Таким образом, $\norm{\vec{c_{p}}} = \sqrt{\frac{\tau w_i}{s_i}}$.

                \item Сдвинем сгенерированный кластер $S_i$ так, чтобы его центр совпал с $\vec{c_{p}}$. Это приведёт к тому, что его вклад станет равен $s_i \norm{c_p}^2 = s_i \sqrt{\frac{\tau w_i}{s_i}} ^ 2 = \tau w_i$.
                \item Если коллайдер кластера не пересекается ни с одним коллайдером уже размещённых кластеров (\textit{процедура проверки этого условия описана в разделе \texttt{2.2.5}}), фиксируем его положение, прерываем цикл и переходим к следующему значению $i$ и обработке следующего кластера.
                \item Иначе, вычисляем значение $\tau_p \in \left(\tau, \infty\right)$ такое, при котором после применения процедуры растяжения (\textit{описана в разделе \texttt{2.2.6}}), коллайдер размещённого кластера не будет пересекать ни одного другого. \textit{Процесс вычисления такого $\tau_p$ описан в разделе \texttt{2.2.7}}. Записываем значение $\tau_p$ в список $\tau_{probes}$.
            \end{enumerate}
        \item Если мы исчерпали все $probes$ проб, так и не найдя подходящей (той, при применении которой не возникает пересечения коллайдеров), найдём минимальный элемент списка $\tau_{probes}$, произведём процедуру растяжения, установив его в качестве нового значения $\tau$: $\tau = \min{\tau_{probes}}$ и перейдём к следующему значению $i$ и размещению следующего кластера.
    \end{enumerate}
\end{enumerate}
\end{algorithm*}

\subsection{Проверка пересечения коллайдеров}

Даны один эллипсоидный коллайдер $E$ и множество $M$ попарно непересекающихся эллипсоидных коллайдеров. Необходимо определить, пересекается ли он хотя бы с одним эллипсоидом из этого множества. Опишем процедуру подобной проверки.

\begin{algorithm*}[H]
\caption{\texttt{CHECK\_CLUSTERS\_COLLISION(E, M)}}

\begin{enumerate}
    \item Сопоставим каждому эллипсоидному коллайдеру два $n$-шара: вписанный и описанный. Найти их просто: пусть коллайдер представлен эллипсоидом $El_n\left(D, V, \vec{c}\right)$, тогда его описывает шар $Cir\left(El_n\left(D, V, \vec{c}\right)\right) = \mathbb{B}_{n}\left(\max_{i \in \left[1, n\right]}{D_{ii}}, \vec{c}\right)$, вписан же в него шар $Ins\left(El_n\left(D, V, \vec{c}\right)\right) = \mathbb{B}_{n}\left(\min_{i \in \left[1, n\right]}{D_{ii}}, \vec{c}\right)$.
    \item Сформируем множество кандидатов на пересечение: $Cand = \left\{E_M \in M | Cir\left(E_M\right) \cap Cir\left(E\right) \right\}$. Коллайдеры из $M \setminus Cand$ заведомо не пересекаются с $E$, ведь их описанные шары не пересекаются с описанным вокруг коллайдера $E$ шаром.
    \item Если $\exists E_{Cand} \in Cand : Ins\left(E_{Cand}\right) \cap Ins\left(E\right)$, то коллайдер $E$ пересекается с $E_{Cand}$, ввиду чего процедуру следует завершить.
    \item Иначе, если не нашлось такого $E_{Cand}$, необходимо проитерироваться по всем $El_{Cand} \in Cand$ и проверить их на пересечение с $E$ с использованием алгоритма, описанного в разделе \texttt{2.2.8}
\end{enumerate}
\end{algorithm*}

\begin{note}
    Первые три шага процедуры являются оптимизационными и теоритически не обязательными. На практике же подобные проверки позволяют быстро отбросить заведомо непересекающиеся коллайдеры. Временная сложность проверки двух шаров $\mathbb{B}_{n}\left(r_1, \vec{c_1}\right)$ и $\mathbb{B}_{n}\left(r_2, \vec{c_2}\right)$ на наличие пересечения, – $O\left(n\right)$, так как она производится проверкой условия $\norm{\vec{c_1} - \vec{c_2}} \le r_1 + r_2$. Временная сложность проверки эллипсоидов на пересечение кратно превышает $O\left(n\right)$.
\end{note}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.50\textwidth]{imgs/colliders_collision_example_1.png}
    \includegraphics[width=0.49\textwidth]{imgs/colliders_collision_example_2.png}
\end{figure}

\begin{figure}[H]
    \includegraphics[width=0.49\textwidth]{imgs/colliders_collision_example_3.png}
    \includegraphics[width=0.49\textwidth]{imgs/colliders_collision_example_4.png}
    \caption{Пересечение коллайдеров: примеры}
    \label{fig:mesh1}
\end{figure}

Во всех приведённых примерах эллипсоид $E$ представлен под номером $1$, а множество $M$ состоит из остальных эллипсоидов.
\begin{itemize}
    \item В первом примере множество $Cand$ состоит из трёх эллипсоидов $2$, $3$ и $4$. Ни один из вписанных шаров множества $Cand$ не пересекается со вписанным в эллипсоид $E$ шаром, поэтому проводится попарная проверка на пересечение эллипсоидов. Рассматриваются пары $\left(1, 2\right)$, $\left(1, 3\right)$ и $\left(1, 4\right)$. Вне зависимости от порядка проверки, результатом процедуры является \texttt{true}, так как пары эллипсоидов $\left(1, 3\right)$ и $\left(1, 4\right)$ пересекаются. В худшем случае будет произведено не более двух проверок, так как после нахождения пересекающейся пары процедура превентивно завершится.
    \item Во втором примере множество $Cand$ состоит из одного эллипсоида $2$. Проверка пересечения выполняется для эллипсоидов $1$ и $2$ и возвращает \texttt{false} (эллипсоиды не пересекаются).
    \item В третьем примере множество $Cand$ состоит из одного эллипсоида $2$. Вписанный шар эллипсоида $2$ из множества $Cand$ пересекается со вписанным в эллипсоид $E$ шаром, поэтому процедура проверки пересечения завершается досрочно с результатом \texttt{true}.
    \item В четвёртом примере множество $Cand$ состоит из одного эллипсоида $2$. Проверка пересечения выполняется для эллипсоидов $1$ и $2$ и возвращает \texttt{false} (эллипсоиды не пересекаются). В этом примере также демонстрируется то, что во множестве $Cand$ могут существовать пересекающиеся пары описанных вокруг коллайдеров шаров, однако на описанную процедуру это никак не влияет.
\end{itemize}

\subsection{Процедура растяжения}

Пусть в пространстве размещено $m$ кластеров. Если при размещении $\left(m+1\right)$-ого возникло пересечение с остальными, его можно устранить с использованием процедуры растяжения, состоящей из одного шага:

\begin{algorithm*}[H]
\caption{\texttt{STRETCH(E, M, k)}}
\begin{enumerate}
    \item Проитерируемся по всем кластерам (включая размещаемый) и умножим центр каждого на $k$, где $k > 1$ – коэфициент растяжения: $\vec{c} = k \vec{c_0}$.
\end{enumerate}
\end{algorithm*}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.49\textwidth]{imgs/scaling_example_1.png}
    \includegraphics[width=0.49\textwidth]{imgs/scaling_example_2.png}
    \caption{Пример: растяжение с коээфициентом $1.65$}
    \label{fig:mesh1}
\end{figure}

\begin{statement}[Инвариантность относительных вкладов]
    Процедура растяжения не изменяет относительных вкладов.
\end{statement}

\begin{proof}
Пусть до растяжения в пространстве находилось множество кластеров $C_0 = \left\{\left(s_i, \vec{c_i}^0\right)\right\}_{i = 1}^{\abs{T}}$. В ходе растяжения, центр каждого кластера изменился: $\vec{c_i} = k \vec{c_i}^0$, что отразилось на его вкладе: $w_i = s_i \norm{\vec{c_i}}^2 = s_i k \norm{\vec{c_i}^0}^2 = k^2 w_i^0 \sim w_i^0$. Таким образом, абсолютный вклад каждого кластера увеличился в $k^2$ раза, но относительный вклад остался неизменным.
\end{proof}

\begin{theorem}
Количество пар коллайдеров, кластеры которых пересекаются не увеличивается в результате растяжения.
\end{theorem}

\begin{proof}
Рассмотрим после растяжения точку $\vec{p}$, лежащую в пересечении двух эллипсоидов $E_1 = El_n\left(D_1, V_1, \vec{c_1}\right)$ и $E_2 = El_n\left(D_2, V_2, \vec{c_2}\right)$. Это означает, что:

\begin{equation*}
\begin{cases}
    \norm{\left(V_1D_1\right)^{-1} \left(\vec{p} - \vec{c_1}\right)} \le 1\\
    \norm{\left(V_2D_2\right)^{-1} \left(\vec{p} - \vec{c_2}\right)} \le 1
\end{cases}
\end{equation*}

\noindentРассмотрим прообраз точки $\vec{p}$: $\vec{p_0} = k^{-1} \vec{p}$ и прообраз эллипсоидов $E_1^0 = El_n\left(D_1, V_1, k^{-1} \vec{c_1}\right)$ и $E_2^0 = El_n\left(D_2, V_2, k^{-1} \vec{c_2}\right)$. Подставим их в полученную систему:

\[
\begin{array}{lcr}
  \begin{cases}
    \norm{\left(V_1D_1\right)^{-1} \left(k \vec{p_0} - k \vec{c_1}^0\right)} \le 1\\
    \norm{\left(V_2D_2\right)^{-1} \left(k \vec{p_0} - k \vec{c_2}^0\right)} \le 1\\
\end{cases} &
\Rightarrow &
\begin{cases}
    \norm{\left(V_1D_1\right)^{-1} \left(\vec{p_0} - \vec{c_1}^0\right)} \le 1\\
    \norm{\left(V_2D_2\right)^{-1} \left(\vec{p_0} - \vec{c_2}^0\right)} \le 1\\
\end{cases}
\end{array}
\]

\noindentИз чего следует, что прообраз каждой точки, лежащей в пересечении коллайдеров $E_1$ и $E_2$ лежит в прообразе этих коллайдеров $E_1^0$ и $E_2^0$, а значит прообраз всех точек, лежащих в пересечении вложен во множество точек пересечения до растяжения. Таким образом, нового пересечения (то есть того, которое бы не имело прообраза) возникнуть не может.
\end{proof}

\begin{theorem}
Существует коэффициент $k_M$ такой, что при растяжении на него, количество пар кластеров, коллайдеры которых пересекаются, равно нулю.
\end{theorem}

\begin{proof}
Пусть в пространстве размещено множество кластеров, которому соответствует множество коллайдеров $C = \left\{El_n\left(D_i, V_i, \vec{c_i}^0\right)\right\}_{i = 1}^{m}$, которому в свою очередь соответствует множество описанных шаров $B = \left\{\mathbb{B}_n\left(r_i, \vec{c_i}^0\right)\right\}_{i = 1}^{m}$. Докажем, что $k_M = \max_{i, j \in \left[1, m\right], i \neq j} \frac{r_i + r_j}{\norm{\vec{c_i}^0 - \vec{c_j}^0}} + \epsilon$ достаточно.

Вспомним, что критерий пересечения двух шаров $\mathbb{B}_n\left(r_1, \vec{c_1}\right)$ и $\mathbb{B}_n\left(r_2, \vec{c_2}\right)$, – $\norm{\vec{c_1} - \vec{c_2}} \le r_1 + r_2$. После же растяжения с коэффициентом $k_M$, для любой пары шаров, описывающих коллайдеры будет выполняться: $\norm{\vec{c_i} - \vec{c_j}} = k_M \norm{\vec{c_i}^0 - \vec{c_j}^0} = \left(\max_{a, b \in \left[1, m\right], a \neq b} \frac{r_a + r_b}{\norm{\vec{c_a}^0 - \vec{c_b}^0}} + \epsilon\right) \norm{\vec{c_i}^0 - \vec{c_j}^0} \ge \left(\frac{r_i + r_j}{\norm{\vec{c_i}^0 - \vec{c_j}^0}} + \epsilon\right) \norm{\vec{c_i}^0 - \vec{c_j}^0} \ge r_i + r_j$.

Таким образом, ни одна пара описанных вокруг коллайдеров шаров не пересекается, а значит и сами коллайдеры попарно непересекаются после растяжения.
\end{proof}

\subsection{Нахождение оптимального $\tau_{probe}$}

Даны кластер $E$, множество $M$ кластеров, коллайдеры которых не пересекаются. Текущий коэффициент растяжения равен $\tau$. Необходимо приближенно (\textit{с точностью не менее заданного заранее достаточно малого $\eps$}) найти такой $\tau_{probe}$, при котором не будет ни одной пары кластеров, коллайдеры которых пересекаются. Применим бинарный поиск:

\begin{algorithm*}[H]
\caption{\texttt{FIND\_TAU(E, M, tau, epsilon)}}
\begin{enumerate}
    \item Запустим процедуру \texttt{CHECK\_COLLIDERS\_COLLISION(E, M)}. Если она вернула \texttt{false}, вернём в качестве ответа $\tau_{probe} = \tau$, иначе перейдём к следующему шагу
    \item Установим $k_l = 1$, $k_r = k_M$, где $k_M$ – коэффициент, приведённый в формулировке \texttt{теоремы 3} для множества $M$
    \item Установим $k = \frac{k_l + k_r}{2}$, сформируем \texttt{E', M' = STRETCH(E, M, k)} и запустим процедуру \texttt{CHECK\_COLLIDERS\_COLLISION(E', M', k)}
    \item Если процедура вернула \texttt{false}, устанавливаем $k_r = k$, иначе устанавливаем $k_l = k$
    \item Если $k_r - k_l \le \eps$, переходим к последнему шагу процедуры, иначе переходим к третьему шагу
    \item Возвращаем в качестве ответа $\tau_{probe} = k_r \tau$
\end{enumerate}
\end{algorithm*}

\begin{statement}
    Пусть $\tau_{opt}$ – точное значение $\tau_{probe}$, а $\tau_{proc}$ – значение, найденное в ходе вышеописанной процедуры. Тогда: $0 \le \tau_{proc} - \tau_{opt} \le \eps$.
\end{statement}

\begin{proof}
    Утверждение очевидно в случае, если процедура \texttt{FIND\_OPTIMAL\_TAU} была прервана в результате выполнения первого шага.
    В противном случае, заметим, что $k_l$ и $k_r$ подобраны таким образом, чтобы $\forall k \le k_l :$ \texttt{CHECK\_COLLIDERS\_COLLISION(E, M, k)} = \texttt{true} и $\forall k \ge k_r :$ \texttt{CHECK\_COLLIDERS\_COLLISION(E, M, k)} = \texttt{false}. Значит, $k_l \le k_{opt} \le k_r$, а $k_r - k_l \le \eps$ для терминальных $k_l$ и $k_r$, вследствие чего $0 \le k_{proc} - k_{opt} \le \eps$, так как $k_{proc} = k_r$ для терминального $k_r$.
\end{proof}

\subsection{Тестирование двух эллипсоидов на пересечение}
Задача о тестировании двух $n$-мерных шаров на пересечение решается проверкой простейшего соотношения: $\norm{\vec{c_1} - \vec{c_2}} \le r_1 + r_2$, проверить которое можно за $\Theta\left(n\right)$. Аналогичная задача для двух соосных $n$-мерных брусов вида $[a_1, b_1] \times [a_2, b_2] \times \dots \times [a_n, b_n]$ решается за $O\left(n\right)$ проверкой системы из $n$ неравентств. Однако, для более сложных фигур, решение, пусть даже приближенное, найти сложнее. Безусловно, существует точный способ поиска пересечения в случае $n = 2$ и приближенные способы в случае $n = 3$, однако, для произвольного $n$, простого и эффективного решения до сих пор в литературе описано не было. В этом разделе главе я привожу алгоритм тестирования двух $n$-мерных эллипсоидов на пересечение с использованием метода градиентного спуска.

\begin{problem}
Пусть в $n$-мерном Евклидовом пространстве заданы два эллипсоида: $El_n\left(D_i, V_i, \vec{c_i}\right)$, где $i \in \{1, 2\}$. Необходимо определить, пересекаются ли эти эллипсоиды или нетъ. Формально: $El_n\left(D_1, V_1, \vec{c_1}\right) \cap El_n\left(D_2, V_2, \vec{c_2}\right) \neq \emptyset$?
\end{problem}

\begin{theorem}
    Преобразование системы координат вида $\vec{x_2} = \left(V D\right)^{-1} \left(\vec{x_1} - \vec{c}\right)$ переводит $El_n(D_1, V_1, \vec{c_1})$ в $El_n(D_2, V_2, \vec{c_2})$, где $\vec{c_2} = \left(VD\right)^{-1}\left(\vec{c_1} - \vec{c}\right)$, а $V_2 D_2 = \left(VD\right)^{-1} \left(V_1D_1\right)$.
\end{theorem}

\begin{proof}
Запишем уравнение эллипсоида в системе координат $\vec{x_1}$:
\begin{equation*}
    \norm{\left(V_1D_1\right)^{-1} \left(\vec{x_1} - \vec{c_1}\right)} \le 1
\end{equation*}

\noindent Подставим $\vec{x_2} = \left(V D\right)^{-1} \left(\vec{x_1} - \vec{c}\right)$:
\begin{equation*}
    \norm{\left(\left(VD\right)^{-1}\left(V_1D_1\right)\right)^{-1} \left(\vec{x_2} - \left(VD\right)^{-1}\left(\vec{c_1} - \vec{c}\right)\right)} \le 1
\end{equation*}

\noindent Обозначим $\vec{c_2} = \left(VD\right)^{-1}\left(\vec{c_1} - \vec{c}\right)$ и $V_2 D_2 = \left(VD\right)^{-1} \left(V_1D_1\right)$ и получим уравнение эллипсоида в системе координат $\vec{x_2}$:

\begin{equation*}
    \norm{\left(V_2D_2\right)^{-1} \left(\vec{x_2} - \vec{c_2}\right)} \le 1
\end{equation*}
\end{proof}

\begin{corollary}
Рассмотренное преобразование переводит $El_n\left(D, V, \vec{c_1}\right)$ в $El_n\left(E, E, \vec{0}\right) = \mathbb{B}_n\left(1, \vec{0}\right)$, то есть в единичный $n$-шар с центром в нуле.
\end{corollary}

\begin{proof}
В этом случае $\left(VD\right)^{-1} \left(V_1D_1\right) = E$, из чего следует $V_2 = D_2 = E$, ну а $\vec{c_2} = \left(VD\right)^{-1}\left(\vec{c} - \vec{c}\right) = \vec{0}$.
\end{proof}

\begin{theorem}[О замечательной точке]
В условиях задачи 2, рассмотрим случай $El_1 = El_n(E, E, \vec{0})$, $El_2 = El_n(D, V, \vec{c})$ Пусть $t = min_{\norm{\left(VD\right)^{-1}\left(\vec{x} - \vec{c}\right)} \le 1} \norm{\vec{x}}$. Тогда $t \le 1 \Leftrightarrow El_1 \cap El_2 \neq \emptyset$.
\end{theorem}

\begin{proof}
Пусть $\vec{x_0} = argmin_{\norm{\left(VD\right)^{-1}\left(\vec{x} - \vec{c}\right)} \le 1} \norm{\vec{x}}$. Напомню, что условие $\norm{\left(VD\right)^{-1}\left(\vec{x} - \vec{c}\right)} \le 1$ равносильно $\vec{x} \in El_2$.

\noindent $\Longrightarrow$ Очевидно, так как по определению $\vec{x_0} \in El_2$, а ввиду $t = \norm{\vec{x_0}} \le 1$, $\vec{x_0} \in El_1$

\noindent $\Longleftarrow$ Пусть не так, то есть $t = \norm{\vec{x_0}} > 1$. В таком случае, $\vec{x_0} \notin El_1$. Рассмотрим произвольную точку $\vec{x_1} \in El_1 \cap El_2$. Очевидно, $\norm{\vec{x_1}} \le 1 < \norm{\vec{x_0}}$, но это противоречит условию минимальности нормы точки $\vec{x_0}$ в эллипсоиде $El_2$, так как $\vec{x_1}$ также принадлежит эллипсоиду $El_2$.
\end{proof}

\begin{corollary}
Рассмотрим упрощённые условия задачи 2, а именно случай $El_1 = El(E, E, \vec{0})$, $El_2 = El(D, V, \vec{c})$. Пусть $t = argmin_{\norm{\vec{x}} \le 1} \norm{\left(VD\right)\vec{x} + \vec{c}}$. Тогда $t \le 1 \Leftrightarrow El_1 \cap El_2 \neq \emptyset$.
\end{corollary}

\begin{proof}
Для доказательства достаточно рассмотреть преобразование, обратное к описанному в \texttt{следствии теоремы 4}, и учесть его непрерывность. Технически доказательство повторяет доказательство \texttt{теоремы 5}.
\end{proof}

\begin{algorithm*}[H]
\caption{\texttt{ELLIPSOIDS\_INTERSECTION\_DETECTION(el1, el2)}}

\begin{enumerate}
    \item Применим преобразование, описанное в \texttt{следствии теоремы 4}
    \item Используем градиентный спуск внутри единичного шара для поиска значения $t$ из \texttt{следствия теоремы 5}:
    \begin{enumerate}
        \item Начнём процедуру в нуле: $\vec{x} = \vec{0}$
        \item Установим $\vec{x}_{next} = \vec{x} - \lambda \nabla \norm{\left(VD\right)\vec{x} + \vec{c}} = \vec{x} - \lambda \left(VD\right)^T \frac{\left(VD\right)\vec{x} + \vec{c}}{\norm{\left(VD\right)\vec{x} + \vec{c}}}$, где $\lambda$ – достаточно маленький коэфициент шага
        \item Если $\norm{\vec{x}_{next}} > 1$, нормализуем вектор $\vec{x}_{next}$, установив его значение $\frac{\vec{x}_{next}}{\norm{\vec{x}_{next}}}$ и возвращая его в границы единичного шара
        \item Если $\norm{\vec{x}_{next} - \vec{x}} \le \eps$, останавливаем процедуру и устанавливаем значение $t = \norm{\left(VD\right)\vec{x}_{next} + \vec{c}}$ и переходим к третьему шагу
        \item Иначе, устанавливаем $\vec{x} = \vec{x}_{next}$ и переходим к шагу \texttt{b}
    \end{enumerate}
    \item Если $t \le 1$, возвращаем \texttt{true} (эллипсоиды пересекаются), иначе \texttt{false} (эллипсоиды не пересекаются). Корректность обуславливается результатом \texttt{теоремы 4} и \texttt{теоремы 5}.
\end{enumerate}
\end{algorithm*}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.49\textwidth]{imgs/gradboost_example_1.png}
    \includegraphics[width=0.49\textwidth]{imgs/gradboost_example_2.png}
    \caption{Тест пересечения эллипсоидов: примеры}
    \label{fig:mesh1}
\end{figure}

В каждом из двух приведённых примеров продемонстрированы два изображения: в исходном пространстве и после преобразования системы координат. Красной линией отмечен путь, проделанный в ходе поиска оптимума функционала, а синим градиентом показано значение самого функционала. В первом примере поиск закончился с положительным результатом, во втором – с отрицательным.

\begin{note}[Оптимальный $\lambda$]
Использование адаптивного (динамически изменяемого) значения $\lambda = \frac{\left(VD\right)\vec{x} + \vec{c}}{\norm{VD}^2}$ позволяет снизить длину пути, проделанного во время градиентного спуска с $109.63$ до $4.79$ (результат получен усредненинем $1000$ экспериментов, проделанных автором).
\end{note}

\begin{note}[Критерий остановки]
Нет необходимости искать точное значение $t$, остановить процедуру можно при нахождении первой же точки $\vec{x} \in El_1 \cap El_2$.
\end{note}

\section{Демонстрация}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{imgs/generator_demo_1.png}
    \includegraphics[width=0.45\textwidth]{imgs/generator_demo_2.png}
    \includegraphics[width=0.45\textwidth]{imgs/generator_demo_4.png}
    \includegraphics[width=0.45\textwidth]{imgs/generator_demo_3.png}
    \caption{Примеры генерируемых кластерных структур}
    \label{fig:mesh1}
\end{figure}

Генератор работает с произвольной размерностью пространства и произвольными параметрами вкладов и размеров кластеров. Среднее время генерации кластерной структуры для $n = 3, N = 5 \cdot 10^4, K = 20$ составляет $1.032 \pm 0.142$ секунды (усреднено в результате $100$ экспериментов).
